# Dotfiles for Sway Running on Thinkpad T480S

Sway is a tiling Wayland compositor and a drop-in replacement for the i3 window manager for X11. It works with your existing i3 configuration and supports most of i3's features, plus a few extras.

![ScreenShot](screenshot.png)

# Details

Below is a list of some of the main packages I use

- **Operating System** --- [Archlinux](https://www.archlinux.org/)
- **AUR Helper** --- [Yay](https://aur.archlinux.org/packages/yay/)
- **Boot Loader** --- [Grub](https://wiki.archlinux.org/index.php/GRUB)
- **Resource Monitor** --- [Btop](https://aur.archlinux.org/packages/btop-git)
- **Window Manager** --- [Sway](https://aur.archlinux.org/packages/sway-git)
- **Bar** --- [Waybar](https://aur.archlinux.org/packages/waybar-git)
- **Screenshots** --- [Grim](https://aur.archlinux.org/packages/grim-git)
- **Idle Management Daemon** --- [Swayidle](https://aur.archlinux.org/packages/swayidle.git)
- **Shell** --- [Bash](https://wiki.archlinux.org/index.php/Bash) 
- **Terminal** --- [Alacritty](https://archlinux.org/packages/community/x86_64/alacritty/)
- **Notification Daemon** --- [Mako](https://aur.archlinux.org/packages/mako-git)
- **Application Launcher** --- [Rofi Wayland](https://aur.archlinux.org/packages/rofi-lbonn-wayland-git)
- **File Manager** --- [Ranger](https://aur.archlinux.org/packages/ranger-git)
- **Image Viewer** --- [Imv](https://archlinux.org/packages/community/x86_64/imv)
- **Editor** --- [Neovim](https://aur.archlinux.org/packages/neovim-git)
  - **Plugins**
	- [gruvbox](https://github.com/morhetz/gruvbox)
	- [coc.nvim](https://github.com/neoclide/coc.nvim)
	- [vim-polyglot](https://github.com/sheerun/vim-polyglot)
	- [vim-startify](https://github.com/mhinz/vim-startify)
	- [tagbar](https://github.com/preservim/tagbar)
	- [nerdtree](https://github.com/preservim/nerdtree)
	- [vim-devicons](https://github.com/ryanoasis/vim-devicons)
	- [vim-airline](https://github.com/vim-airline/vim-airline)
	- [vim-airline-themes](https://github.com/vim-airline/vim-airline-themes)
	- [indentLine](https://github.com/Yggdroot/indentLine)
	- [vim-hexokinase](https://github.com/rrethy/vim-hexokinase)
- **Sound** --- [Pulseaudio](https://archlinux.org/packages/extra/x86_64/pulseaudio/)
- **PDF Viewer** --- [Zathura](https://wiki.archlinux.org/index.php/Zathura)
- **IRC** --- [Weechat](https://weechat.org/)
- **RSS Feed Reader** --- [Newsboat](https://aur.archlinux.org/packages/newsboat-git)
- **Youtube search** --- [Ytfzf](https://aur.archlinux.org/packages/ytfzf)
- **Youtube Downloader** --- [Yt-dlp (youtube-dl fork)](https://aur.archlinux.org/packages/yt-dlp-git) with [Yt-dlp-drop-in](https://aur.archlinux.org/packages/yt-dlp-drop-in)
- **Video player** --- [Mpv](https://aur.archlinux.org/packages/mpv-git)
